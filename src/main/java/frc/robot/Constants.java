package frc.robot;

public class Constants {

    // Controller constants here
    public class ControllerConstants {

        // Mobility Controller
        static final int MOBILITY_CONTROLLER_ID = 0;
        static final int YAW_AXIS_ID = 4;
        static final int MAIN_AXIS_ID = 1;
        static final int STRAFE_AXIS_ID = 0;

        // Manipulator Controller
        static final int MANIPULATOR_CONTROLLER_ID = 1;
        static final int SHOULDER_AXIS_ID = 1;
        static final int ELBOW_AXIS_ID = 4;
        static final int CLAW_AXIS_ID = 5;
        
        static final double DEADZONE = 0.15;
    }

    // Drive constants here
    public class DriveConstants {

        // Drive motor controller IDs go here. NOTE: Maybe should be on Settings page on Dashboard?
        public static final int LEFT_FRONT_MOTOR_ID = 1;
        public static final int RIGHT_FRONT_MOTOR_ID = 2;

        public static final int LEFT_REAR_MOTOR_ID = 5;
        public static final int RIGHT_REAR_MOTOR_ID = 6;

        public static final double MAIN_AXIS_SPEED = 0.95;
        public static final double STRAFE_AXIS_SPEED = 1.00;
        public static final double YAW_AXIS_SPEED = 0.75;
        public static final double RAMP_TIME = 0.00;
        public static final double DEADZONE = 0.30;
        
        // public static final double DEADZONE = 0.15; // Last year's value was 0.15
    }

    // Arm movement constants here
    public class ArmConstants {
        public static final int SHOULDER_MOTOR1_ID = 7;
        public static final int SHOULDER_MOTOR2_ID = 8;
        public static final int ELBOW_MOTOR_ID = 9;
        public static final int CLAW_MOTOR_ID = 10;
    }
}
